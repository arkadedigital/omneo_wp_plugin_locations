<?php namespace Omneo\Locations;

use Omneo\Core;

defined('ABSPATH') or die('Access Denied');

/**
 * Plugin Name:     Omneo Locations
 * Plugin URI:      http://www.omneo.com.au
 * Version:         1.0.6
 * Description:     Omneo locations plugin
 */

// Load acf json files
Core\register_acf_from_json(__PATH());

// Load post types
require_once(__PATH() . '/lib/post-types.php');

// Load functions
require_once(__PATH() . '/lib/functions.php');

function __PATH()
{
    return plugin_dir_path(__FILE__);
}