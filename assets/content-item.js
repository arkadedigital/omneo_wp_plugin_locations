angular.module('locations', []);
angular.element(document).ready(function () {
    angular.bootstrap(document.getElementById('ng-locations'), ['locations']);
});

angular.module('locations').controller('LocationsCtrl', function ($scope, locationsService, $timeout) {
    $scope.locations = [];
    $scope.selected_locations = [];

    // Wait for $scope.content_item_id to initialize
    $timeout(function () {
        // Get content item locations
        if ($scope.content_item_id) {
            locationsService.getContentLocations({content_item_id: $scope.content_item_id}).then(function (data) {
                _.each(data, function (content_item_location) {
                    var location = _.find($scope.locations, function (l) {
                        return l.id == content_item_location.location_id;
                    });

                    if(location) {
                        content_item_location.title = location.title;

                        // Default action to 'add' if lookbook or gallery content type
                        if($scope.content_type === 'lookbooks' || $scope.content_type === 'gallery') {
                            content_item_location.action = 'add';
                        }
                    } else {
                        content_item_location.title = 'Location does not exist';
                    }
                });

                $scope.selected_locations = data;
                $scope.is_loaded = true;
            });
        } else {
            $scope.is_loaded = true;
        }
    });

    $scope.add_selected_location = function (location_id) {

        // If added already, don't add again
        var location_exists = _.find($scope.selected_locations, function (selected_location) {
            return selected_location.location_id == location_id;
        });
        if (location_exists) {
            location_exists.action = '';
            return false;
        }

        var location = _.find($scope.locations, function (l) {
            return l.id == location_id;
        });
        $scope.selected_locations.push({id: location.id, location_id: location_id, title: location.title, action: 'add'});
    };

    $scope.delete_location = function (location) {
        if (location.action !== 'add') {
            location.action = 'delete';
        } else {
            $scope.selected_locations = _.without($scope.selected_locations, location);
        }
    };
});


/**
 * Locations service
 */
angular.module('locations').factory('locationsService', function ($http, $q) {
    return {
        /**
         * Get content item locations
         * @returns {*}
         */
        getContentLocations: function (args) {
            return $http({
                url: ajaxurl,
                method: 'POST',
                params: {
                    action: 'locations_send_request',
                    data: {
                        verb: 'get',
                        api_request: 'contentitemlocations',
                        data: args
                    }
                }
            }).then(function (response) {
                if (typeof response.data.error !== 'undefined') {
                    return $q.reject(response.data.error.message);
                } else if (response.data === 'null') {
                    return $q.reject('Omneo API Error');
                }

                return response.data.data;
            });
        }
    };
});