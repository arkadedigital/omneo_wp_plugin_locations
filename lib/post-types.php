<?php defined('ABSPATH') or die('Access Denied');

// Location post type
function omneo_location_post_type()
{
    // creating (registering) the custom type
    register_post_type('locations', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
        // let's now add all the options for this post type
        array('labels' => array(
            'name' => __('Locations'), /* This is the Title of the Group */
            'singular_name' => __('Location'), /* This is the individual type */
            'all_items' => __('All Locations'), /* the all items menu item */
            'add_new' => __('Add New'), /* The add new menu item */
            'add_new_item' => __('Add New Location'), /* Add New Display Title */
            'edit' => __('Edit'), /* Edit Dialog */
            'edit_item' => __('Edit Locations'), /* Edit Display Title */
            'new_item' => __('New Location'), /* New Display Title */
            'view_item' => __('View the Location'), /* View Display Title */
            'search_items' => __('Search Location'), /* Search Custom Type Title */
            'not_found' => __('Nothing found in the Database.'), /* This displays if there are no entries yet */
            'not_found_in_trash' => __('Nothing found in Trash'), /* This displays if there is nothing in the trash */
            'parent_item_colon' => ''
        ), /* end of arrays */
            'description' => __('This is the main "Locations" section containing the locations list on the App'), /* Custom Type Description */
            'public' => true,
            'publicly_queryable' => false,
            'exclude_from_search' => false,
            'show_ui' => true,
            'query_var' => true,
            'menu_position' => 20, /* this is what order you want it to appear in on the left hand side menu */
            'menu_icon' => 'dashicons-location-alt', /* the icon for the view type menu */
            'rewrite' => array('slug' => 'locations', 'with_front' => false), /* you can specify its url slug */
            'has_archive' => 'locations', /* you can rename the slug here */
            'capability_type' => 'post',
            'hierarchical' => false,
            /* the next one is important, it tells what's enabled in the post editor */
            'supports' => array('title')
        ) /* end of options */
    ); /* end of register post type */
}
add_action('init', __NAMESPACE__ . '\\omneo_location_post_type');