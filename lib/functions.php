<?php namespace Omneo\Locations;

use Omneo\Core;

defined('ABSPATH') or die('Access Denied');


/**
 * On saving post (create or edit) make api call to Omneo
 *
 * @param $post_id
 * @param $post
 * @param $updated
 */
add_action('save_post', __NAMESPACE__ . '\\save_post', 10, 3);
function save_post($post_id, $post, $updated)
{

    if ($post->post_type == 'locations' && $updated) {
        $omneo_id = get_field('omneo_id', $post_id);
        $address = get_field('address', $post_id);
        $latitude = get_field('latitude', $post_id);
        $longitude = get_field('longitude', $post_id);
        $phone_number = get_field('phone_number', $post_id);
        $country = get_field('country', $post_id);
        $region = get_field('region', $post_id);
        $postcode = get_field('postcode', $post_id);
        $opening_hours = get_field('opening_hours', $post_id);
        $location_type = get_field('type', $post_id);

        // Data
        $args['data'] = [
            // Required
            'title' => $post->post_title,
            // Optional
            'address' => $address,
            'latitude' => $latitude,
            'longitude' => $longitude,
            'phone' => $phone_number,
            'country' => $country,
            'region' => $region,
            'postcode' => $postcode,
            'opening_hours' => $opening_hours,
            'location_type' => $location_type
        ];

        $args['api_request'] = ($omneo_id ? 'locations/' . $omneo_id : 'locations');
        $args['verb'] = $omneo_id ? 'put' : 'post';

        // Make request
        $response = Core\send_request($args);


        if (isset($response['error'])) {
            // API Error
            $class = "alert alert-danger";
            $message = $response['error']['message'];
            echo "<div class=\"$class\"> <p>$message</p></div>";
        } elseif (!$omneo_id) { // Create
            // Update omneo id
            update_field('field_56149e7c42a02', $response['data']['id'], $post_id);
        }
    }
}



/**
 * Load assets
 */
function load_assets()
{
    if((isset($_GET['post']) && get_post_type($_GET['post']) == 'locations') || (isset($_GET['post_type']) && $_GET['post_type'] == 'locations')) {
        wp_enqueue_script('locations', plugins_url() . DS . 'omneo-locations' . DS . 'assets' . DS . 'main.js');
    }

    // Content item locations metabox
    if((isset($_GET['post']) && get_post_type($_GET['post']) == 'content_items') || (isset($_GET['post_type']) && $_GET['post_type'] == 'content_items')) {
        wp_enqueue_script('angular', plugins_url() . DS . 'omneo-core' . DS . 'assets' . DS . 'angular.min.js');
        wp_enqueue_script('locations', plugins_url() . DS . 'omneo-locations' . DS . 'assets' . DS . 'content-item.js');
    }
}
load_assets();


// Setup content item metabox
function omneo_locations_post_meta_box_setup()
{
    add_meta_box(
        'content-item-locations',           // Unique ID
        esc_html__('Locations', 'locations'),   // Title
        __NAMESPACE__ . '\\content_item_locations',       // Callback function
        'content_items',                // Admin page (or post type)
        'side',                       // Context
        'default'                       // Priority
    );
}
add_action('load-post.php', __NAMESPACE__ . '\\omneo_locations_post_meta_box_setup');
add_action('load-post-new.php', __NAMESPACE__ . '\\omneo_locations_post_meta_box_setup');


/**
 * Locations view
 */
function content_item_locations()
{
    // Load metabox
    require_once(__PATH() . '/lib/content-item.php');
}

function locations_send_request()
{
    $data = json_decode(stripslashes($_GET['data']), true);
    $response = Core\send_request($data);
    $json = json_encode($response);
    echo $json;
    exit;
}

add_action('wp_ajax_locations_send_request', __NAMESPACE__ . '\\locations_send_request');


function delete_location($pid)
{
    $post = get_post($pid);
    $omneo_id = get_field('omneo_id', $pid);



    $args = [
        'api_request' => 'locations/' . $omneo_id,
        'verb' => 'delete'
    ];

    $response = Core\send_request($args);


}

add_action('before_delete_post', __NAMESPACE__ . '\\delete_location', 10, 3);

