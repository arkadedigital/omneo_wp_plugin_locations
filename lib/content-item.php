<?php
global $CONTENT_TYPE_LIST;
$content_type = $CONTENT_TYPE_LIST[get_field('content_type', $_GET['post'])];
$content_item_id = $content_type ? \Omneo\Content\get_content_item_id(get_field('omneo_id', $_GET['post']), $content_type) : '';
?>


<div id="ng-locations">
    <div ng-controller="LocationsCtrl" ng-cloak ng-init="content_item_id='<?php echo $content_item_id ?>'; content_type='<?php echo $content_type; ?>'">
        <?php $query = new \WP_Query(['post_type' => 'locations', 'post_status' => 'publish']); ?>

        <table style="width:100%;" ng-show="is_loaded">
            <tr>
                <td>
                    <select style="width:100%;" ng-model="selected_location">
                        <option value="">Select location</option>
                        <?php if ($query->have_posts()): $index = 0; ?>
                            <?php while ($query->have_posts()): $query->the_post(); ?>
                                <?php if ($omneo_id = get_field('omneo_id', get_the_ID())): ?>
                                    <option value="<?php echo $omneo_id; ?>"
                                            ng-init="locations[<?php echo $index; ?>]['id']=<?php echo $omneo_id; ?>;locations[<?php echo $index; ?>]['title']='<?php the_title(); ?>'">
                                        #<?php echo $omneo_id; ?> - <?php print_r(the_title()); ?>
                                    </option>
                                    <?php $index++; endif; ?>
                            <?php endwhile; ?>
                        <?php endif;
                        wp_reset_postdata(); ?>
                    </select>
                </td>
                <td>
                    <button type="button" class="button" ng-disabled="!selected_location"
                            ng-click="add_selected_location(selected_location)">+
                    </button>
                </td>
            </tr>
        </table>


        <table ng-if="selected_locations.length>0">
            <tr ng-repeat="location in selected_locations" ng-show="location.action !== 'delete'">
                <td>
                    #{{ location.location_id }} {{ location.title }}
                </td>
                <td>
                    <div class="dashicons dashicons-dismiss" ng-click="delete_location(location)"></div>
                    <input type="hidden" name="locations[{{ $index }}][action]" value="{{ location.action }}"
                           ng-disabled="!location.action">
                    <input type="hidden" name="locations[{{ $index }}][id]" value="{{ location.id }}"
                           ng-disabled="!location.action || content_type==='lookbooks' || content_type==='gallery'">
                    <input type="hidden" name="locations[{{ $index }}][id]" value="{{ location.location_id }}"
                           ng-disabled="content_type!=='lookbooks' && content_type!=='gallery'">
                </td>
            </tr>
        </table>

        <div ng-if="selected_locations.length===0 && is_loaded">No locations</div>
        <div ng-if="!is_loaded"><span class="spinner is-active" style="float:none;margin-top:0;"></span> Loading
            locations...
        </div>
    </div>
</div>


